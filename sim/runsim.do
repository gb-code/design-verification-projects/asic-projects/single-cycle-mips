vlib work
vlog ../rtl/*.v
vlog *.v
vsim -t ps -L work -novopt mips_tb
log -r *
do wave.do
run -all
