// ----------------------------------------------------------------------------
// File Name     : mips_tb.v
// Author        : Gaurav
// Date          : 11/13/16
// Module Name   : mips_tb
// Revision      : 1.0
// Description   : This module is Testbench Top module for verification of
//                 MIPS processor.
// ----------------------------------------------------------------------------
`timescale 1ns/100ps

module mips_tb ();

reg  CLK    ;
reg  RST_N  ;

mips_top U_mips_top (.CLK (CLK), .RST_N (RST_N));

initial
begin
  CLK   = 1'b0  ;
  RST_N = 1'b0  ;
  #20 ;
  RST_N = 1'b1  ;
  repeat(256) @ (posedge CLK);
  $finish ;
end

always #5 CLK = ~CLK ;

initial
begin
  @(posedge RST_N);
  forever
  begin
    @ (posedge CLK) ;
    if(U_mips_top.INSTR === 32'bx)
    begin
      $display("Fetched Instruction is X, So Finishing the Simulation");
      $finish ;
    end
    else
      $display("INSTR: PC: 0x%08h OPCODE: %06b FUNCT : %06b",U_mips_top.PC,
                                 U_mips_top.INSTR[31:26], U_mips_top.INSTR[5:0]);
  end
end

always @ (posedge CLK)
  if(U_mips_top.dmem_MemWen)
    $display("Store Into Data MEM ADDR=%08h DATA=%08h",U_mips_top.MemAddr, U_mips_top.MemWrData);
  
`ifdef NCSIM
initial
begin
  $recordfile("mips_tb");
  $recordvars("*");
end
`endif

endmodule
