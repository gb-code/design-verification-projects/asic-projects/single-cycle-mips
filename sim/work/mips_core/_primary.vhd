library verilog;
use verilog.vl_types.all;
entity mips_core is
    port(
        CLK             : in     vl_logic;
        RST_N           : in     vl_logic;
        INSTR           : in     vl_logic_vector(31 downto 0);
        MemRdData       : in     vl_logic_vector(31 downto 0);
        PC              : out    vl_logic_vector(31 downto 0);
        MemAddr         : out    vl_logic_vector(31 downto 0);
        MemWrData       : out    vl_logic_vector(31 downto 0);
        MemWen          : out    vl_logic
    );
end mips_core;
