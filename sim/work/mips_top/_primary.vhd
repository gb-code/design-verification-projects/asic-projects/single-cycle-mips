library verilog;
use verilog.vl_types.all;
entity mips_top is
    port(
        CLK             : in     vl_logic;
        RST_N           : in     vl_logic
    );
end mips_top;
