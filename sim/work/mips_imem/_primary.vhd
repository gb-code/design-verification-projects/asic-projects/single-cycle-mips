library verilog;
use verilog.vl_types.all;
entity mips_imem is
    generic(
        IMEM_DEPTH      : integer := 65536
    );
    port(
        A               : in     vl_logic_vector(31 downto 0);
        RD              : out    vl_logic_vector(31 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of IMEM_DEPTH : constant is 1;
end mips_imem;
