library verilog;
use verilog.vl_types.all;
entity mips_dmem is
    generic(
        DMEM_DEPTH      : integer := 65536;
        DMEM_ADDRW      : integer := 16
    );
    port(
        CLK             : in     vl_logic;
        A               : in     vl_logic_vector;
        WE              : in     vl_logic;
        WD              : in     vl_logic_vector(31 downto 0);
        RD              : out    vl_logic_vector(31 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of DMEM_DEPTH : constant is 1;
    attribute mti_svvh_generic_type of DMEM_ADDRW : constant is 1;
end mips_dmem;
