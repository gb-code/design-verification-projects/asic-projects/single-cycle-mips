library verilog;
use verilog.vl_types.all;
entity mips_addr_dec is
    generic(
        ADDR_DEC_MSBS   : integer := 24
    );
    port(
        MemWen          : in     vl_logic;
        MemAddr         : in     vl_logic_vector(31 downto 0);
        dmem_MemWen     : out    vl_logic;
        gpio_MemWen     : out    vl_logic;
        fact_MemWen     : out    vl_logic;
        fmul_MemWen     : out    vl_logic;
        RdSel           : out    vl_logic_vector(1 downto 0)
    );
    attribute mti_svvh_generic_type : integer;
    attribute mti_svvh_generic_type of ADDR_DEC_MSBS : constant is 1;
end mips_addr_dec;
