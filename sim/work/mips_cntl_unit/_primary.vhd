library verilog;
use verilog.vl_types.all;
entity mips_cntl_unit is
    port(
        OpCode          : in     vl_logic_vector(5 downto 0);
        Funct           : in     vl_logic_vector(5 downto 0);
        MemtoReg        : out    vl_logic;
        MemWrite        : out    vl_logic;
        Branch          : out    vl_logic;
        ALUSrc          : out    vl_logic;
        RegDst          : out    vl_logic;
        Jump            : out    vl_logic;
        Link            : out    vl_logic;
        ImmtoReg        : out    vl_logic;
        RegWrite        : out    vl_logic;
        JumpReg         : out    vl_logic;
        ShtoReg         : out    vl_logic;
        LotoReg         : out    vl_logic;
        HitoReg         : out    vl_logic;
        LoWrite         : out    vl_logic;
        HiWrite         : out    vl_logic;
        ALUControl      : out    vl_logic_vector(2 downto 0);
        ShControl       : out    vl_logic_vector(2 downto 0)
    );
end mips_cntl_unit;
