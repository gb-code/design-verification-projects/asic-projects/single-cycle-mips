onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider {MIPS Core}
add wave -noupdate -radix hexadecimal /mips_tb/U_mips_top/CLK
add wave -noupdate -radix hexadecimal /mips_tb/U_mips_top/RST_N
add wave -noupdate -radix hexadecimal /mips_tb/U_mips_top/PC
add wave -noupdate -radix hexadecimal /mips_tb/U_mips_top/INSTR
add wave -noupdate -radix hexadecimal /mips_tb/U_mips_top/MemAddr
add wave -noupdate -radix hexadecimal /mips_tb/U_mips_top/MemWrData
add wave -noupdate -radix hexadecimal /mips_tb/U_mips_top/MemWen
add wave -noupdate -radix hexadecimal /mips_tb/U_mips_top/MemRdData
add wave -noupdate -divider {Main Decoder}
add wave -noupdate /mips_tb/U_mips_top/U_mips_core/ImmtoReg
add wave -noupdate /mips_tb/U_mips_top/U_mips_core/Link
add wave -noupdate /mips_tb/U_mips_top/U_mips_core/Jump
add wave -noupdate /mips_tb/U_mips_top/U_mips_core/RegDst
add wave -noupdate /mips_tb/U_mips_top/U_mips_core/ALUSrc
add wave -noupdate /mips_tb/U_mips_top/U_mips_core/Branch
add wave -noupdate /mips_tb/U_mips_top/U_mips_core/RegWrite
add wave -noupdate /mips_tb/U_mips_top/U_mips_core/MemtoReg
add wave -noupdate -divider {Auxiliary Decoder}
add wave -noupdate /mips_tb/U_mips_top/U_mips_core/HiWrite
add wave -noupdate /mips_tb/U_mips_top/U_mips_core/LoWrite
add wave -noupdate /mips_tb/U_mips_top/U_mips_core/HitoReg
add wave -noupdate /mips_tb/U_mips_top/U_mips_core/LotoReg
add wave -noupdate /mips_tb/U_mips_top/U_mips_core/ShtoReg
add wave -noupdate /mips_tb/U_mips_top/U_mips_core/JumpReg
add wave -noupdate /mips_tb/U_mips_top/U_mips_core/ALUControl
add wave -noupdate /mips_tb/U_mips_top/U_mips_core/ShControl
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {184046 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {75508 ps} {269758 ps}
