##
## INTRODUCTION

This work aims at the design and implementation of a single cycle MIPS processor and functional verification of the same so it can be used in later point of time to integrate with different peripherals. The MIPS processor is implemented with basic arithmetic instruction, jump instructions, shift instructions and multiplication instructions.
To verify this single cycle MIPS system, a basic code and a factorial code is used.

![1.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/fbb7a589-0faa-44e3-ae0a-b5ecf3bd1308)


![2.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/d7c95dcb-7567-4e6b-ae47-8219fb05ddaa)


![3.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/fe85cc53-9974-4beb-8f7c-9be04e3c9510)

##
## MIPS VERIFICATION STRATEGY

MIPS processor has been verified by using simple programs like combination of different instructions and a Factorial calculation recursive subroutine. These programs have been written in assembly language and converted to hexadecimals using the free MIPS assemblers available (MARS). Then this Hexadecimal code has been loaded in Instruction memory ROM using $readmemh Verilog construct. The program will run once the RST_N has been released and the final output will have monitored by looking at the Memory write transactions to data memory.

##
## SIMULATION RESULTS

![4.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/c8908d14-4e4c-4b0b-adae-c7eb510d5f85)

![5.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/1eeb2c80-6dba-439d-bd6a-e4c39afc72d6)

![6.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/9aae06bb-9f34-4b7d-8a75-2f3ee9d4b94b)


![7.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/e1aa7cf5-d950-4523-9973-c04287523263)

![8.jpg](https://images.zenhubusercontent.com/5a1b59d28a75884b9088d234/97d2953c-dc58-4635-bd29-136c6c8f096a)
