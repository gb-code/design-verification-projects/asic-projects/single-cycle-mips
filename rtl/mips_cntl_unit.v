// ----------------------------------------------------------------------------
// File Name     : mips_cntl_unit.v
// Author        : Gaurav
// Date          : 11/13/16
// Module Name   : mips_cntl_unit
// Revision      : 1.0
// Description   : This module implements the MIPS processor Control unit
//                 Which Comprises of Main and Auxillary Decoding units
// ----------------------------------------------------------------------------

module mips_cntl_unit (
                       input  wire  [5:0]    OpCode     ,
                       input  wire  [5:0]    Funct      ,

                       output wire           MemtoReg   ,
                       output wire           MemWrite   ,
                       output wire           Branch     ,
                       output wire           ALUSrc     ,
                       output wire           RegDst     ,
                       output wire           Jump       ,
                       output wire           Link       ,
                       output wire           ImmtoReg   ,

                       output reg            RegWrite   ,
                       output wire           JumpReg    ,
                       output wire           ShtoReg    ,
                       output wire           LotoReg    ,
                       output wire           HitoReg    ,
                       output wire           LoWrite    ,
                       output wire           HiWrite    ,
                       output wire  [2:0]  ALUControl ,
                       output wire  [2:0]  ShControl  
                      );

reg [10:0] MainDecCntlWord ;    
wire       MainRegWrite    ;
wire [1:0]  AUXOp           ;
reg [11:0] AuxDecCntlWord  ;    

// Defining a Control word for Main Decoder and Auxillary Decoder
// Main Decoder 1:0 - Aux Op
//              2   - MainRegWrite
//              3   - MemtoReg
//              4   - MemWrite
//              5   - Branch
//              6   - ALUSrc
//              7   - RegDst
//              8   - Jump 
//              9   - Link 
//             10   - ImmtoReg
assign {ImmtoReg, Link    , Jump        , RegDst, ALUSrc, Branch,
        MemWrite, MemtoReg, MainRegWrite, AUXOp } = MainDecCntlWord ;

always @ (*)
begin
  casez(OpCode)
    6'b000000 : MainDecCntlWord = 11'b0_0_0_1_0_0_0_0_1_10 ;   // R-Type Instructions
    6'b100011 : MainDecCntlWord = 11'b0_0_0_0_1_0_0_1_1_00 ;   // LW
    6'b101011 : MainDecCntlWord = 11'b0_0_0_0_1_0_1_0_0_00 ;   // SW
    6'b000100 : MainDecCntlWord = 11'b0_0_0_0_0_1_0_0_0_01 ;   // Branch
    6'b001000 : MainDecCntlWord = 11'b0_0_0_0_1_0_0_0_1_00 ;   // ADDI
    6'b000010 : MainDecCntlWord = 11'b0_0_1_0_0_0_0_0_0_00 ;   // Jump Instruction
    6'b000011 : MainDecCntlWord = 11'b0_1_1_0_0_0_0_0_1_00 ;   // JAL - Jump and Link
    6'b001111 : MainDecCntlWord = 11'b1_0_0_0_1_0_0_0_1_00 ;   // LUI - Load Upper Immediate
    default   : MainDecCntlWord = 11'b0_0_0_0_0_0_0_0_0_00 ;   // Default - NOP
  endcase
end

// Auxillary Decoder
// AUXOp - 00 - ADD (Used for LW address)
//         01 - SUB (Used for Branch)
//         10 - Used for R-Type. Use Function for Futher Decoding
//         11 - Reserved
// Auxillary Control Word 2:0 - ALUControl
//                              000 - AND
//                              001 - OR
//                              010 - ADD
//                              011 -
//                              100 - XOR 
//                              101 - NOR
//                              110 - SUB
//                              111 - SLT
//                        5:3 - ShControl
//                              000 - Left Shift
//                              001 - 
//                              010 - Right Shift
//                              011 - Right Shift Sign Extension
//                              100 - Left Shift - Vectored
//                              101 - 
//                              110 - Right Shift - Vectored
//                              111 - Right Shift Sign Extension - Vectored
//                          6 - JumpReg
//                          7 - ShtoReg
//                          8 - LotoReg
//                          9 - HitoReg
//                         10 - LoWrite
//                         11 - HiWrite
assign {HiWrite, LoWrite, HitoReg, LotoReg, ShtoReg, JumpReg, ShControl, ALUControl} = AuxDecCntlWord ;
always @ (*)
begin
  casez({AUXOp, Funct})
    8'b00_?????? : begin   // Immediate Instructions
                     AuxDecCntlWord = 12'b0_0_0_0_0_0_000_010 ;
                     RegWrite       = MainRegWrite           ;
                   end
    8'b01_?????? : begin   // Branch Instructions
                     AuxDecCntlWord = 12'b0_0_0_0_0_0_000_110 ;
                     RegWrite       = MainRegWrite           ;
                   end
    8'b10_100100 : begin   // R-Type Instruction -- AND
                     AuxDecCntlWord = 12'b0_0_0_0_0_0_000_000 ;
                     RegWrite       = MainRegWrite           ;
                   end
    8'b10_100101 : begin   // R-Type Instruction -- OR
                     AuxDecCntlWord = 12'b0_0_0_0_0_0_000_001 ;
                     RegWrite       = MainRegWrite           ;
                   end
    8'b10_100110 : begin   // R-Type Instruction -- XOR
                     AuxDecCntlWord = 12'b0_0_0_0_0_0_000_100 ;
                     RegWrite       = MainRegWrite           ;
                   end
    8'b10_100111 : begin   // R-Type Instruction -- NOR
                     AuxDecCntlWord = 12'b0_0_0_0_0_0_000_101 ;
                     RegWrite       = MainRegWrite           ;
                   end
    8'b10_100000 : begin   // R-Type Instruction -- ADD
                     AuxDecCntlWord = 12'b0_0_0_0_0_0_000_010 ;
                     RegWrite       = MainRegWrite           ;
                   end
    8'b10_100010 : begin   // R-Type Instruction -- SUB
                     AuxDecCntlWord = 12'b0_0_0_0_0_0_000_110 ;
                     RegWrite       = MainRegWrite           ;
                   end
    8'b10_011001 : begin   // R-Type Instruction -- MULTU
                     AuxDecCntlWord = 12'b1_1_0_0_0_0_000_000 ;
                     RegWrite       = 1'b0                   ;
                   end
    8'b10_001000 : begin   // R-Type Instruction -- JR
                     AuxDecCntlWord = 12'b0_0_0_0_0_1_000_000 ;
                     RegWrite       = 1'b0                   ;
                   end
    8'b10_000000 : begin   // R-Type Instruction -- SLL
                     AuxDecCntlWord = 12'b0_0_0_0_1_0_000_000 ;
                     RegWrite       = MainRegWrite           ;
                   end
    8'b10_000010 : begin   // R-Type Instruction -- SRL
                     AuxDecCntlWord = 12'b0_0_0_0_1_0_010_000 ;
                     RegWrite       = MainRegWrite           ;
                   end
    8'b10_000011 : begin   // R-Type Instruction -- SRA
                     AuxDecCntlWord = 12'b0_0_0_0_1_0_011_000 ;
                     RegWrite       = MainRegWrite           ;
                   end
    8'b10_000100 : begin   // R-Type Instruction -- SLLV
                     AuxDecCntlWord = 12'b0_0_0_0_1_0_100_000 ;
                     RegWrite       = MainRegWrite           ;
                   end
    8'b10_000110 : begin   // R-Type Instruction -- SRLV
                     AuxDecCntlWord = 12'b0_0_0_0_1_0_110_000 ;
                     RegWrite       = MainRegWrite           ;
                   end
    8'b10_000111 : begin   // R-Type Instruction -- SRAV
                     AuxDecCntlWord = 12'b0_0_0_0_1_0_111_000 ;
                     RegWrite       = MainRegWrite           ;
                   end
    8'b10_101010 : begin   // R-Type Instruction -- SLT
                     AuxDecCntlWord = 12'b0_0_0_0_0_0_000_111 ;
                     RegWrite       = MainRegWrite           ;
                   end
    8'b10_010000 : begin   // R-Type Instruction -- MFHI
                     AuxDecCntlWord = 12'b0_0_1_0_0_0_000_000 ;
                     RegWrite       = MainRegWrite            ;
                   end
    8'b10_010010 : begin   // R-Type Instruction -- MFLO
                     AuxDecCntlWord = 12'b0_0_0_1_0_0_000_000 ;
                     RegWrite       = MainRegWrite            ;
                   end
    default      : begin   // Default
                     AuxDecCntlWord = 12'b0_0_0_0_0_0_000_000 ;
                     RegWrite       = 1'b0                    ;
                   end
  endcase
end


endmodule
