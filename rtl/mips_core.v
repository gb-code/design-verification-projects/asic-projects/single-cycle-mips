// ----------------------------------------------------------------------------
// File Name     : mips_core.v
// Author        : Gaurav
// Date          : 11/13/16
// Module Name   : mips_core
// Revision      : 1.0
// Description   : This module implements the MIPS core which implements the
//                 Datapath & Control unit which constructs the MIPS processor
// ----------------------------------------------------------------------------

module mips_core (
                  input  wire                 CLK       ,
                  input  wire                 RST_N     ,
                  input  wire  [31:0]         INSTR     ,
                  input  wire  [31:0]         MemRdData ,
                  output wire  [31:0]         PC        ,
                  output wire  [31:0]         MemAddr   ,
                  output wire  [31:0]         MemWrData ,
                  output wire                 MemWen    
                 );
                
// Program Counter
wire         MemtoReg   ;
wire         Branch     ;
wire         ALUSrc     ;
wire         RegDst     ;
wire         Jump       ;
wire         Link       ;
wire         ImmtoReg   ;
wire         RegWrite   ;
wire         JumpReg    ;
wire         ShtoReg    ;
wire         LotoReg    ;
wire         HitoReg    ;
wire         LoWrite    ;
wire         HiWrite    ;
wire  [2:0]  ALUControl ;
wire  [2:0]  ShControl  ;
wire [31:0] PCNext    ;
wire [31:0] PCNext1   ;
wire [31:0] PCNext2   ;
wire [31:0] PCBranch  ;
wire [31:0] PCPlus4   ;
wire [31:0] PCJump    ;
wire [31:0] SignImm   ;
wire [31:0] SrcA      ;
wire [31:0] SrcB      ;
wire [31:0] SrcB1     ;
wire [4:0]  RegRa1    ;
wire [4:0]  RegRa2    ;
wire [4:0]  RegWa     ;
wire [4:0]  RegWa1    ;
wire [31:0] ALUResult ;
wire [31:0] SHResult  ;
wire        ALUZero   ;
wire [31:0] RegWrData1;
wire [31:0] RegWrData2;
wire [31:0] RegWrData3;
wire [31:0] RegWrData4;
wire [31:0] RegWrData5;
wire [31:0] RegWrData ;
wire [31:0] LdImmiVal ;
wire [63:0] MulResult ;
wire [31:0] LoReg     ;
wire [31:0] HiReg     ;


flopr #(.WIDTH(32)) U_PC_REG ( .clk  (CLK     ), .reset(~RST_N  ),
                               .d    (PCNext  ), .q    (PC      )
                             );

// Program Counter Multiplexer Implementation for different Instructions
// JR Instruction PC Mux - Next PC address Comes from the Register contents SrcA
mux2 #(.WIDTH(32)) U_PC_JR_MUX (.d0 (PCNext1  ), .d1(SrcA     ), .s (JumpReg ), .y(PCNext ));

// Jump Instruction PC Mux - Next PC Address comes from the Immediate Instruction and
// Current PCPlus4 MSB
assign PCJump = {PCPlus4[31:28],INSTR[25:0],2'b00} ;
mux2 #(.WIDTH(32)) U_PC_J_MUX  (.d0 (PCNext2  ), .d1(PCJump   ), .s (Jump    ), .y(PCNext1));

// PC Branch Instruction - Next PC Address Comes from the Adding the Sign extended 16-biy
// Immediate value to the Current PC Value
mux2 #(.WIDTH(32)) U_PC_B_MUX  (.d0 (PCPlus4  ), .d1(PCBranch ), .s (PCSrc   ), .y(PCNext2));

adder U_PCBranchAdder (.a ({SignImm[29:0],2'b00}), .b (PCPlus4 ), .y (PCBranch));

// Normal PC Plus 4 Implementation
adder U_PCAdder       (.a (PC                   ), .b (32'd4   ), .y (PCPlus4 ));

// Instrunction Decoding

// Register File
regfile U_regfile (.clk  (CLK          ), .we3 (RegWrite    ), .wd3 (RegWrData ),
                   .ra1  (INSTR[25:21] ), .ra2 (INSTR[20:16]), .wa3 (RegWa     ),
                   .rd1  (SrcA         ), .rd2 (SrcB1       )
                  );

// Destination Register Address Multiplexer Implementation
mux2 #(.WIDTH(5)) U_RegWa_Dst_MUX1 (.d0 (INSTR[20:16]  ), .d1(INSTR[15:11] ),
                                    .s  (RegDst        ), .y (RegWa1       ));
mux2 #(.WIDTH(5)) U_RegWa_Dst_MUX2 (.d0 (RegWa1        ), .d1(5'd31        ),
                                    .s  (Link          ), .y (RegWa        ));

assign MemWrData = SrcB1 ;
// ALU
mux2 #(.WIDTH(32)) U_ALU_SRC_MUX  (.d0 (SrcB1    ), .d1(SignImm  ), .s (ALUSrc  ), .y(SrcB ));
mips_alu U_mips_alu (
                     .a        (SrcA       ),
                     .b        (SrcB       ),
                     .alucont  (ALUControl ),
                     .result   (ALUResult  ),
                     .zero     (ALUZero    )
                );

assign PCSrc = ALUZero & Branch ;
// Sign Extension
signext U_signext (.a(INSTR[15:0]), .y(SignImm) );

// Multiplier
mips_mul U_mips_mul (.a (SrcA ), .b (SrcB ), .result (MulResult ) );

// HiReg LoReg Implementation
flopenr #(.WIDTH(32) ) U_HiReg (
                                .clk     (CLK              ),
                                .reset   (~RST_N           ),
                                .en      (HiWrite          ),
                                .d       (MulResult[63:32] ),
                                .q       (HiReg            )
                              );
flopenr #(.WIDTH(32) ) U_LoReg (
                                .clk     (CLK              ),
                                .reset   (~RST_N           ),
                                .en      (LoWrite          ),
                                .d       (MulResult[31: 0] ),
                                .q       (LoReg            )
                              );
// Shift Operation
mips_shift U_mips_shift (.a       (SrcA       ),
                         .b       (SrcB       ),
                         .shamt   (INSTR[10:6]),
                         .shctrl  (ShControl  ),
                         .result  (SHResult   )
                        );

assign LdImmiVal = {INSTR[15:0],16'b0} ;

// Control Unit
mips_cntl_unit U_mips_cntl_unit (
                .OpCode     (INSTR[31:26]     ),
                .Funct      (INSTR[ 5: 0]     ),

                .MemtoReg   (MemtoReg         ),
                .MemWrite   (MemWen           ),
                .Branch     (Branch           ),
                .ALUSrc     (ALUSrc           ),
                .RegDst     (RegDst           ),
                .Jump       (Jump             ),
                .Link       (Link             ),
                .ImmtoReg   (ImmtoReg         ),

                .RegWrite   (RegWrite         ),
                .JumpReg    (JumpReg          ),
                .ShtoReg    (ShtoReg          ),
                .LotoReg    (LotoReg          ),
                .HitoReg    (HitoReg          ),
                .LoWrite    (LoWrite          ),
                .HiWrite    (HiWrite          ),
                .ALUControl (ALUControl       ),
                .ShControl  (ShControl        )
               );

// Result Selection
assign MemAddr = ALUResult ;
mux2 #(.WIDTH(32)) U_WD_Mux1  (.d0 (ALUResult  ), .d1(MemRdData  ), .s (MemtoReg  ), .y(RegWrData1 ));
mux2 #(.WIDTH(32)) U_WD_Mux2  (.d0 (RegWrData1 ), .d1(LoReg      ), .s (LotoReg   ), .y(RegWrData2 ));
mux2 #(.WIDTH(32)) U_WD_Mux3  (.d0 (RegWrData2 ), .d1(HiReg      ), .s (HitoReg   ), .y(RegWrData3 ));
mux2 #(.WIDTH(32)) U_WD_Mux4  (.d0 (RegWrData3 ), .d1(SHResult   ), .s (ShtoReg   ), .y(RegWrData4 ));
mux2 #(.WIDTH(32)) U_WD_Mux5  (.d0 (RegWrData4 ), .d1(LdImmiVal  ), .s (ImmtoReg  ), .y(RegWrData5 ));
mux2 #(.WIDTH(32)) U_WD_Mux6  (.d0 (RegWrData5 ), .d1(PCPlus4    ), .s (Link      ), .y(RegWrData  ));


endmodule
