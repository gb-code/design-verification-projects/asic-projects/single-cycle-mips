// ----------------------------------------------------------------------------
// File Name     : mips_shift.v
// Author        : Gaurav
// Date          : 11/13/16
// Module Name   : mips_shift
// Revision      : 1.0
// Description   : This module implements the MIPS Logical Shift Operations
// ----------------------------------------------------------------------------

module mips_shift (
                 input  wire  [31:0]   a       ,
                 input  wire  [31:0]   b       ,
                 input  wire  [ 4:0]   shamt   ,
                 input  wire  [ 2:0]   shctrl  ,
                 output reg   [31:0]   result  
                );

wire [4 :0] shamt_int ;
wire [63:0] a_int     ;
wire [63:0] rsh_result;
wire [31:0] lsh_result;

assign shamt_int = shctrl[2] ? b[4:0] : shamt ;
assign lsh_result = (a     << shamt_int) ;
assign a_int      = shctrl[0] ? {{32{a[31]}},a} : {32'b0,a} ;
assign rsh_result = (a_int >> shamt_int) ;

always @ (*)
begin
  casez(shctrl[1:0])
    2'b00   : result = lsh_result ;
    2'b10   ,
    2'b11   : result = rsh_result[31:0] ;
    default : result = 32'b0 ;
  endcase
end
    

endmodule

