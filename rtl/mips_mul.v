// ----------------------------------------------------------------------------
// File Name     : mips_mul.v
// Author        : Gaurav
// Date          : 11/13/16
// Module Name   : mips_mul
// Revision      : 1.0
// Description   : This module implements the MIPS Multiplier
// ----------------------------------------------------------------------------

module mips_mul (
                 input  wire  [31:0]   a       ,
                 input  wire  [31:0]   b       ,
                 output wire  [63:0]   result  
                );

assign result = a * b ;

endmodule

