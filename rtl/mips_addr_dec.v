// ----------------------------------------------------------------------------
// File Name     : mips_addr_dec.v
// Author        : Gaurav
// Date          : 11/13/16
// Module Name   : mips_addr_dec
// Revision      : 1.0
// Description   : This module is used to decode the Address output from MIPS
//                 core and generate write enables for Data memory and 
//                 other peripherals
// ----------------------------------------------------------------------------

`define DMEM_BASE_ADDR 24'h00_0000
`define GPIO_BASE_ADDR 24'h00_0001
`define FACT_BASE_ADDR 24'h00_0002
`define FMUL_BASE_ADDR 24'h00_0003
module mips_addr_dec #(parameter ADDR_DEC_MSBS = 24)
                      (
                       input  wire             MemWen       ,
                       input  wire   [31:0]    MemAddr      ,

                       output wire             dmem_MemWen  ,
                       output wire             gpio_MemWen  ,
                       output wire             fact_MemWen  ,
                       output wire             fmul_MemWen  ,
                       output reg    [ 1:0]    RdSel        
                      );

assign dmem_MemWen = MemWen & (MemAddr[31:(32-ADDR_DEC_MSBS)] == `DMEM_BASE_ADDR) ;
assign gpio_MemWen = MemWen & (MemAddr[31:(32-ADDR_DEC_MSBS)] == `GPIO_BASE_ADDR) ;
assign fact_MemWen = MemWen & (MemAddr[31:(32-ADDR_DEC_MSBS)] == `FACT_BASE_ADDR) ;
assign fmul_MemWen = MemWen & (MemAddr[31:(32-ADDR_DEC_MSBS)] == `FMUL_BASE_ADDR) ;

always @ (*)
begin
  casez(MemAddr[31:(32-ADDR_DEC_MSBS)])
    `DMEM_BASE_ADDR : RdSel = 2'b00 ;
    `GPIO_BASE_ADDR : RdSel = 2'b01 ;
    `FACT_BASE_ADDR : RdSel = 2'b10 ;
    `FMUL_BASE_ADDR : RdSel = 2'b11 ;
    default         : RdSel = 2'b00 ;
  endcase
end

endmodule
