// ----------------------------------------------------------------------------
// File Name     : mips_alu.v
// Author        : Gaurav
// Date          : 11/13/16
// Module Name   : mips_alu
// Revision      : 1.0
// Description   : This module implements the MIPS processor ALU
// ----------------------------------------------------------------------------

module mips_alu (
                 input  wire  [31:0]   a       ,
                 input  wire  [31:0]   b       ,
                 input  wire  [ 2:0]   alucont ,
                 output reg   [31:0]   result  ,
                 output wire           zero    
                );

wire   [31:0]   b2, sum, slt ;
assign b2 = alucont[2] ? ~b : b ;
assign sum = a + b2 + alucont[2] ;
assign slt = sum[31] ? 32'd1 : 32'd0 ;

always @ (*)
begin
  casez(alucont)
    3'b000  : result =   a & b  ;  // AND Operation
    3'b001  : result =   a | b  ;  // OR  Operation
    3'b010  : result =   sum    ;  // ADD Operation
    3'b100  : result =   a ^ b  ;  // XOR Operation
    3'b101  : result = ~(a | b) ;  // NOR Operation
    3'b110  : result =   sum    ;  // SUB Operation
    3'b111  : result =   slt    ;  // SLT Operation
    default : result =   32'b0  ;  // NOP
  endcase
end

assign zero = (result == 32'b0) ; // Zero Flag

endmodule
