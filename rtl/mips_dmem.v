// ----------------------------------------------------------------------------
// File Name     : mips_dmem.v
// Author        : Gaurav
// Date          : 11/13/16
// Module Name   : mips_dmem
// Revision      : 1.0
// Description   : This module implements the data memory in the 
//                 MIPS processor.
// ----------------------------------------------------------------------------

module mips_dmem #(parameter DMEM_DEPTH=65536,
                             DMEM_ADDRW=16
                  ) (
                     input  wire                  CLK,
                     input  wire [DMEM_ADDRW-1:0] A  ,
                     input  wire                  WE ,
                     input  wire [31:0]           WD ,
                     output wire [31:0]           RD 
                    );

reg [31:0] mips_dmem_reg [0:DMEM_DEPTH-1] ;

always @ (posedge CLK) if(WE) mips_dmem_reg[A] <= WD ;

assign RD = mips_dmem_reg[A] ;

endmodule

