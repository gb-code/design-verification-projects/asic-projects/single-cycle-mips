// ----------------------------------------------------------------------------
// File Name     : mips_top.v
// Author        : Gaurav
// Date          : 11/13/16
// Module Name   : mips_top
// Revision      : 1.0
// Description   : This module implements the MIPS top level, which includes
//                 MIPS processor, instruction memory and Data memory
// ----------------------------------------------------------------------------

module mips_top (
                 input  wire        CLK  ,
                 input  wire        RST_N
                );

wire     [31:0]  PC               ;
wire     [31:0]  INSTR            ;

wire     [31:0]  MemAddr          ;
wire     [31:0]  MemWrData        ;
wire             MemWen           ;
reg      [31:0]  MemRdData        ;

wire             dmem_MemWen      ;
wire             gpio_MemWen      ;
wire             fact_MemWen      ;
wire             fmul_MemWen      ;
wire     [31:0]  dmem_MemRdData   ;
wire     [31:0]  gpio_MemRdData   ;
wire     [31:0]  fact_MemRdData   ;
wire     [31:0]  fmul_MemRdData   ;
wire     [ 1:0]  RdSel            ;


// Top level Address Decoder and Read mul Intstantiation
mips_addr_dec #(.ADDR_DEC_MSBS (24)) U_mips_addr_dec
                      (
                       .MemWen       (MemWen       ),
                       .MemAddr      (MemAddr      ),
                                                   
                       .dmem_MemWen  (dmem_MemWen  ),
                       .gpio_MemWen  (gpio_MemWen  ),
                       .fact_MemWen  (fact_MemWen  ),
                       .fmul_MemWen  (fmul_MemWen  ),
                       .RdSel        (RdSel        )
                      );

// Read Multiplexer implementation
always @ (*)
begin
  casez(RdSel)
    2'b00   : MemRdData = dmem_MemRdData ;
    2'b01   : MemRdData = gpio_MemRdData ;
    2'b10   : MemRdData = fact_MemRdData ;
    2'b11   : MemRdData = fmul_MemRdData ;
    default : MemRdData = 32'b0          ;
  endcase
end

mips_dmem #(.DMEM_DEPTH(64),
            .DMEM_ADDRW(6 )
           ) U_mips_dmem (
                          .CLK (       CLK             ),
                          .A   (       MemAddr[7:2]    ),
                          .WE  (  dmem_MemWen          ),
                          .WD  (       MemWrData       ),
                          .RD  (  dmem_MemRdData       )
                         );
                                    
mips_imem #(.IMEM_DEPTH(1024)
           ) U_mips_imem (
                          .A  ({2'b0,PC[31:2]}),
                          .RD (INSTR          )
                         );
 

mips_core U_mips_core (
                       .CLK       (CLK        ),
                       .RST_N     (RST_N      ),
                       .PC        (PC         ),
                       .INSTR     (INSTR      ),
                       .MemAddr   (MemAddr    ),
                       .MemWrData (MemWrData  ),
                       .MemWen    (MemWen     ),
                       .MemRdData (MemRdData  )
                      );
endmodule

