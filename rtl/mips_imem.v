// ----------------------------------------------------------------------------
// File Name     : mips_imem.v
// Author        : Gaurav
// Date          : 11/13/16
// Module Name   : mips_imem
// Revision      : 1.0
// Description   : This module implements the instruction memory in the 
//                 MIPS processor.
// ----------------------------------------------------------------------------

`define MEM_INIT_FILE "mips_program.hex"

module mips_imem #(parameter IMEM_DEPTH=65536)
                  (
                   input  wire [31:0] A  ,
                   output wire [31:0] RD 
                  );

reg [31:0] mips_imem_reg [0:IMEM_DEPTH-1] ;

initial $readmemh(`MEM_INIT_FILE,mips_imem_reg);

assign RD = mips_imem_reg[A] ;

endmodule

